import React, { useState, useEffect } from 'react';
import { View, Text, Button, FlatList, Alert, BackHandler } from 'react-native';
import axios from 'axios';

function App() {
  const [pendientes, setPendientes] = useState([]);
  const [opcion, setOpcion] = useState(null);

  useEffect(() => {
    obtenerListaPendientes();
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Alert.alert('Saliendo de la aplicación...');
      BackHandler.exitApp();
      return true;
    });
    return () => backHandler.remove();
  }, []);

  const obtenerListaPendientes = async () => {
    try {
      const respuesta = await axios.get('http://jsonplaceholder.typicode.com/todos');
      setPendientes(respuesta.data);
    } catch (error) {
      console.error('Error al obtener la lista de pendientes:', error);
    }
  };

  const imprimirPendientes = (listaPendientes) => {
    const result = listaPendientes.map(pendiente => `ID: ${pendiente.id}, Título: ${pendiente.title}`).join('\n');
    Alert.alert('Lista de pendientes:', result);
  };

  const imprimirPendientesPorUserID = (listaPendientes) => {
    const result = listaPendientes.map(pendiente => `ID: ${pendiente.id}, ID de usuario: ${pendiente.userId}`).join('\n');
    Alert.alert('Lista de pendientes (IDs y userID):', result);
  };

  const handleMenuSelection = async (selectedOption) => {
    setOpcion(selectedOption);
    switch (selectedOption) {
      case '1':
        imprimirPendientes(pendientes.map(pendiente => ({ id: pendiente.id })));
        break;
      case '2':
        imprimirPendientes(pendientes);
        break;
      case '3':
        imprimirPendientes(pendientes.filter(pendiente => !pendiente.completed));
        break;
      case '4':
        imprimirPendientes(pendientes.filter(pendiente => pendiente.completed));
        break;
      case '5':
        imprimirPendientesPorUserID(pendientes);
        break;
      case '6':
        imprimirPendientesPorUserID(pendientes.filter(pendiente => pendiente.completed));
        break;
      case '7':
        imprimirPendientesPorUserID(pendientes.filter(pendiente => !pendiente.completed));
        break;
      case '8':
        BackHandler.exitApp();
        break;
      default:
        Alert.alert('Opción no válida. Por favor, seleccione una opción del menú (1-8).');
        break;
    }
  };

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20 }}>
      <Text>==== Menú ====</Text>
      <Button title="Actualizar Lista" onPress={obtenerListaPendientes} />
      <FlatList
        data={pendientes}
        renderItem={renderizarItem}
        keyExtractor={item => item.id.toString()}
        style={{ marginTop: 20, width: '100%' }}
      />
      <View style={{ marginTop: 20 }}>
        <Button title="Lista de todos los pendientes (solo IDs)" onPress={() => handleMenuSelection('1')} />
        <Button title="Lista de todos los pendientes (IDs y Titles)" onPress={() => handleMenuSelection('2')} />
        <Button title="Lista de todos los pendientes sin resolver (ID y Título)" onPress={() => handleMenuSelection('3')} />
        <Button title="Lista de todos los pendientes resueltos (ID y Título)" onPress={() => handleMenuSelection('4')} />
        <Button title="Lista de todos los pendientes (IDs y userID)" onPress={() => handleMenuSelection('5')} />
        <Button title="Lista de todos los pendientes resueltos (ID y userID)" onPress={() => handleMenuSelection('6')} />
        <Button title="Lista de todos los pendientes sin resolver (ID y userID)" onPress={() => handleMenuSelection('7')} />
        <Button title="Salir" onPress={() => handleMenuSelection('8')} />
      </View>
    </View>
  );
}

const renderizarItem = ({ item }) => (
  <View style={{ marginVertical: 5 }}>
    <Text>ID: {item.id}</Text>
    <Text>Título: {item.title}</Text>
    <Text>Completado: {item.completed ? 'Sí' : 'No'}</Text>
  </View>
);

export default App;