const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function obtenerListaPendientes() {
    try {
        const fetch = await import('node-fetch');
        const respuesta = await fetch.default('http://jsonplaceholder.typicode.com/todos');
        const datos = await respuesta.json();
        return datos;
    } catch (error) {
        console.error('Error al obtener la lista de pendientes:', error);
        return [];
    }
}

function imprimirPendientes(pendientes) {
    console.log('Lista de todos los pendientes:');
    pendientes.forEach(pendiente => {
        console.log('ID: ' + pendiente.id + ', Título: ' + pendiente.title);
    });
}

function imprimirPendientesNoResueltos(pendientes) {
    const noResueltos = pendientes.filter(pendiente => !pendiente.completed);
    console.log('Lista de todos los pendientes sin resolver:');
    imprimirPendientes(noResueltos);
}

function imprimirPendientesResueltos(pendientes) {
    const resueltos = pendientes.filter(pendiente => pendiente.completed);
    console.log('Lista de todos los pendientes resueltos:');
    imprimirPendientes(resueltos);
}

function imprimirPendientesPorUserID(pendientes) {
    console.log('Lista de todos los pendientes (IDs y userID):');
    pendientes.forEach(pendiente => {
        console.log('ID: ' + pendiente.id + ', ID de usuario: ' + pendiente.userId);
    });
}

function imprimirPendientesResueltosPorUserID(pendientes) {
    const resueltos = pendientes.filter(pendiente => pendiente.completed);
    console.log('Lista de todos los pendientes resueltos (ID y userID):');
    imprimirPendientesPorUserID(resueltos);
}

function imprimirPendientesNoResueltosPorUserID(pendientes) {
    const noResueltos = pendientes.filter(pendiente => !pendiente.completed);
    console.log('Lista de todos los pendientes sin resolver (ID y userID):');
    imprimirPendientesPorUserID(noResueltos);
}

function mostrarMenu() {
    console.log('==== Menú ====');
    console.log('1. Lista de todos los pendientes (solo IDs)');
    console.log('2. Lista de todos los pendientes (IDs y Titles)');
    console.log('3. Lista de todos los pendientes sin resolver (ID y Título)');
    console.log('4. Lista de todos los pendientes resueltos (ID y Título)');
    console.log('5. Lista de todos los pendientes (IDs y userID)');
    console.log('6. Lista de todos los pendientes resueltos (ID y userID)');
    console.log('7. Lista de todos los pendientes sin resolver (ID y userID)');
    console.log('8. Salir');

    rl.question('Seleccione una opción del menú (1-8): ', async (opcion) => {
        const pendientes = await obtenerListaPendientes();

        switch (opcion) {
            case '1':
                console.log('Lista de todos los pendientes (solo ID):');
                pendientes.forEach(pendiente => console.log(`ID: ${pendiente.id}`));
                break;
            case '2':
                console.log('Lista de todos los pendientes (IDs y Titles):');
                imprimirPendientes(pendientes);
                break;
            case '3':
                imprimirPendientesNoResueltos(pendientes);
                break;
            case '4':
                imprimirPendientesResueltos(pendientes);
                break;
            case '5':
                console.log('Lista de todos los pendientes (IDs y userID):');
                imprimirPendientesPorUserID(pendientes);
                break;
            case '6':
                imprimirPendientesResueltosPorUserID(pendientes);
                break;
            case '7':
                imprimirPendientesNoResueltosPorUserID(pendientes);
                break;
            case '8':
                console.log('Saliendo de la aplicación...');
                rl.close();
                break;
            default:
                console.log('Opción no válida. Por favor, seleccione una opción del menú (1-8).');
                break;
        }

        if (opcion !== '8') {
            mostrarMenu();
        }
    });
}

mostrarMenu();